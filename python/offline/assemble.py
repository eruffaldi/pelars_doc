from collections import defaultdict
ts=[(int(x.split("\t")[0]),x.split("\t")[-1]) for x in """SessionID	PLAN	RESULT	OVERALL	TEAM
1542	ok	ok	good	A
1552	ok	good	good	E
1565	ok	good	good	A
1609	good	good	good	D
1576	ok	good	good	E
1585	good	good	good	A
1602	ok	good	good	E
1544	ok	bad	ok	C
1557	bad	ok	ok	F
1564	ok	ok	ok	B
1606	bad	ok	ok	F
1568	bad	ok	ok	C
1584	bad	bad	ok	B
1600	ok	good	ok	D
1543	good	bad	bad	B
1550	ok	bad	bad	D
1599	good	bad	bad	C
1593	ok	bad	bad	F""".split("\n")[1:]]

of = open("out.html","wb")

oo = defaultdict(list)

for s,g in ts:
	oo[g].append(s)
of.write("<table>")
for a in "ABCDEF":
	o = oo[a]
	of.write("<tr><td colspan='3'>Group %s Sessions %d %d %d</td></tr>" % (a,o[0],o[1],o[2]))
	of.write("<tr><td><img src='screen%d.png'></td><td><img src='screen%d.png'></td><td><img src='screen%d.png'></td></tr>" % (o[0],o[1],o[2]))
of.close()