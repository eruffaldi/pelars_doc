import os,sys,argparse,json
from subprocess import check_output
import itertools,pprint
from PIL import Image,ImageOps,ImageFont,ImageDraw
import datetime
COLORG = '\x1b[6;30;42m' 
COLOR0 = '\x1b[0m'
import paramiko
def getvideoinfo(vm):
    fr = 0
    du = 0
    p = ["ffprobe","-v","error","-show_format","-show_streams",vm]
    #print " ".join(p)
    try:
        for line in check_output(p,stderr=None,shell=False).split("\n"):
            if line.startswith("duration="):
                x = line.split("=",1)[1]
                if x != "N/A":
                    du = float(x)
                else:
                    du = -2
            elif line.startswith("nb_frames="):
                x = line.split("=",1)[1]
                if x != "N/A":
                    fr = int(x)
                else:
                    fr = -2
    except:
        print sys.exc_info()
        fr = -1
        du = -1
    return (fr,du)




def enforcewidth(im,w,h):
    if im.size[0] > w:
        hs = im.size[1]*w/im.size[0]
        return im.resize((w,hs),Image.ANTIALIAS)
    else:
        return im

class ApproxSync:
    def __init__(self,keys,slops_ms,queue_size):
        self.listener = lambda x: 0
        self.iqueues = dict([(k,i) for i,k in enumerate(keys)])
        self.queues = [{} for k in keys]
        self.slops_ms = slops_ms
        self.discarded = dict([(k,0) for i,k in enumerate(keys)])
        self.queue_size = queue_size
        self.seen = dict([(k,0) for i,k in enumerate(keys)])
    def signalMessage(self,msgs):
        self.listener(msgs)
    #https://github.com/strawlab/image_pipeline/blob/master/camera_calibration/src/camera_calibration/approxsync.py
    def push(self,key,time_sec,content):
        # TODO assume always latest
        q = self.queues[self.iqueues[key]]
        q[time_sec] = content
        d = 0
        self.seen[key] += 1
        while len(q) > self.queue_size:
            del q[min(q)]       
            d += 1
        if d > 0:
            self.discarded[key] += d
        for vv in itertools.product(*[q.keys() for q in self.queues]):
            if ((max(vv) - min(vv)) < self.slops_ms):
                msgs = [q[t] for q,t in zip(self.queues, vv)]
                self.signalMessage(msgs)
                for q,t in zip(self.queues, vv):
                    try:
                        del q[t]
                    except KeyError:
                        pass # TODO: why can del q[t] fail?
def main():

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--good',action="store_true")
    parser.add_argument('--ucv',action="store_true")
    parser.add_argument('--assemble',action="store_true")
    parser.add_argument('--upload',action="store_true")
    parser.add_argument('--generate',action="store_true")
    parser.add_argument('--makehtml',action="store_true")
    parser.add_argument('--password')
    args = parser.parse_args()
    allsessions = []
    p = os.environ.get("PELARSDATA","/Volumes/BigData/PELARS/sessions")
    if not args.good and not args.ucv:
        print "ALL downloaded"
        for s in os.listdir(p):
            fp = os.path.join(p,s)
            if s[0] == "S" and os.path.isdir(fp):
                allsessions.append(int(s[1:]))
    elif not args.ucv:
        print "GOOD only"
        for s in json.load(open(os.path.join(p,"goodsessions.json"),"rb")):
            allsessions.append(s["session"])
    else:
        print "UCV"
        for q in open(os.path.join(p,"session_scores.csv"),"rb"):
            a = q.split("\t")
            if a[0] != "SessionID":
                allsessions.append(int(a[0]))

    acc = dict(host="pelars.sssup.it",user="poweredge",password=args.password,base="/home/poweredge/uploads/session_videos/")
    if args.upload:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect( acc["host"], username = acc["user"], password = acc["password"], timeout=10)
        sftp = ssh.open_sftp()
        for s in allsessions:
            fp = os.path.join(p,"S%d" % s)
            fpm = os.path.join(fp,"mediaalign")
            fpf = os.path.join(fp,"mediaalign.mp4")
            if os.path.isdir(fpm) and os.path.isfile(fpf):
                rpf = "%s/%d/snapshots.mp4" % (acc["base"],s)
                try:
                    fs = sftp.stat(rpf).st_size
                except:
                    fs = -1
                    try:
                        sftp.mkdir("%s/%d" % (acc["base"],s))
                    except:
                        pass
                lfs = os.stat(fpf).st_size
                print s,fs,lfs
                if fs != lfs:
                    print "need copy",fpf,rpf
                    sftp.put(fpf,rpf,confirm=True)
                    #sshcopy(acc,direction="toremote",local=fpf,remote=rpf)
    elif args.assemble:
        #
        for s in allsessions:
            fp = os.path.join(p,"S%d" % s)
            fpm = os.path.join(fp,"mediaalign")
            fpf = os.path.join(fp,"mediaalign.mp4")
            if os.path.isdir(fpm) and not os.path.isfile(fpf):
                print "run",fpm
                os.system("ffmpeg -r 1 -framerate 1 -i %s/a_%%04d.jpg -q:v 0 -r 1 -framerate 1 -c:v libx264 -pix_fmt yuv420p %s" % (fpm,fpf))
    elif args.makehtml:
        lastfiles = []
        for s in allsessions:
            fp = os.path.join(p,"S%d" % s)
            fpm = os.path.join(fp,"mediaalign")
            m = os.listdir(fpm)
            lastfile = m[len(m)/2]
            lastfiles.append((s,os.path.join(fpm,lastfile)))
        out = []
        for s,fp in lastfiles:
            out.append("<img width='800' src='%s'><br/>" % fp)
        open("output.html","wb").write("\n".join(out))


    elif args.generate:
        bordery = 32
        fnt = ImageFont.truetype('Verdana.ttf', bordery)
        for s in allsessions:
            fp = os.path.join(p,"S%d" % s)
            if os.path.isdir(fp):
                #
                mm = fp + "_multimedias.json"
                if os.path.isfile(mm):
                    w = json.load(open(mm,"rb"))


                    aligned = []

                    approxsync = ApproxSync(("people","workspace","screen"),slops_ms=30000,queue_size=3)
                    approxsync.listener = lambda x: aligned.append(x)
                    # first and last
                    for x in w:
                        if not ("view" in x and x.get("trigger","") == "automatic"):
                            continue
                        v = x["view"]
                        t = x["time"]
                        id = x["id"]
                        content = "%d.%s" % (id,x["mimetype"])
                        x["content"] = content
                        if not os.path.isfile(os.path.join(fp,"media",content)):
                            print "missing",content
                        else:
                            approxsync.push(v,int(t),x)
                    print "----Session",s
                    #print "\n".join([" ".join((a[0]["content"],a[1]["content"],a[2]["content"])) for a in aligned])
                    #print "\n".join(["%d %d" % (a[1]["time"]-a[0]["time"],a[2]["time"]-a[0]["time"]) for a in aligned])
                    for k,v in approxsync.seen.iteritems():
                        if v == 0:
                            print s,"NOT seen",k
                    for k,v in approxsync.discarded.iteritems():
                        if v > 0:
                            print s,"discarded",k,v
                    last = None
                    ll = [aligned[i][0]["time"]-aligned[i-1][0]["time"] for i in range(1,len(aligned))]
                    print [x for x in ll if x > 61000]
                    # TODO: stich the images png/jpeg into one single horizontal (different resolutions) and then make video 1min
                    #
                    # people 800x448
                    # workspace 1920x1080
                    # screen 1280x1024
                    #
                    # horizontal overall: 800+1280+1920 with max vertical 1080 an scaled
                    pa = os.path.join(fp,"mediaalign")
                    h = 576
                    w = 1024
                    bordery = 20
                    rh = h +bordery                
                    if not os.path.isdir(pa):
                        os.makedirs(pa)
                    dt0 = None
                    for i,a in enumerate(aligned):
                        ff = [ enforcewidth(Image.open(os.path.join(fp,"media",x["content"])),w,h) for x in a]
                        dt = datetime.datetime.fromtimestamp(a[0]["time"]/1000.0)
                        if dt0 is not None:
                            dts = (dt-dt0).seconds
                        else:
                            dts = 0
                            dt0 = dt
                        new_im = Image.new('RGB', (3*w,h+bordery)) #(800+1920+1280,h))
                        draw = ImageDraw.Draw(new_im)
                        draw.text((0,int(bordery*0.8)), "%6ds - %s" % (dts,dt.isoformat()), font=fnt, fill=(255,255,255,0))

                        # p=800 w=1920 s=1280
                        new_im.paste(ff[0], ((w-ff[0].size[0])/2,(h-ff[0].size[1])/2+bordery))
                        new_im.paste(ff[1], (w+(w-ff[1].size[0])/2,(h-ff[1].size[1])/2+bordery))
                        new_im.paste(ff[2], (2*w+(w-ff[2].size[0])/2,(h-ff[1].size[1])/2+bordery))
                        #print os.path.join(pa,"a_%04d.jpg" % i)
                        new_im.save(os.path.join(pa,"a_%04d.jpg" % i))
                        # load the three files
                        # stick matrices in opencv
                        # save file
                        # prepare script for ffmpeg make video






if __name__ == '__main__':
    main()