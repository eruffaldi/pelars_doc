from pelarslas import *
import json,argparse

def init():
	c = Connection()
	if not c.token:
	    c.login("giacomo.dabisias@gmail.com","Spe4840he")
	return c

if __name__ == '__main__':


	parser = argparse.ArgumentParser(description='Manipulate Session')
	parser.add_argument('--clear', action="store_true",help="clear operations")
	parser.add_argument('--presence', action="store_true",help="computes presence")
	parser.add_argument('--list', action="store_true",help="provides list of ops")
	parser.add_argument("-S", dest="session",type=int,default=1051,help="specifies session")
 

	args = parser.parse_args()
	c = init()
	session = args.session
	if args.clear:
		c.clearops(session)
	if args.presence:
		r = c.hasop(session,"presence")
		if r is None:
			print "new needed"
			r = c.oppresence(session,"presence")
			print "new",r.text,r.status_code
		else:
			print "exists",json.loads(r[0]["result"])
	if args.list:
		r = c.getops(session)
		print r