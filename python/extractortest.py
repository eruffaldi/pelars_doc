#!/usr/bin/python
import extractor
import argparse
import pprint
from pelarslas import *
import numpy as np

def windowparse(s):
    try:
        x, y = map(int, s.split(','))
        return x, y
    except:
        raise argparse.ArgumentTypeError("Window is in seconds: overlap,rest")


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-s","--session",nargs="+",default=[1600],type=int)
	parser.add_argument("--good",action="store_true")
	parser.add_argument("--sessionscores",action="store_true")
	parser.add_argument("--phases",action="store_true")
	parser.add_argument("--byphase",action="store_true")
	parser.add_argument("--bywindow",type=int)
	parser.add_argument("--bywindowover",type=windowparse)
	parser.add_argument("--skipempty",type=bool,default=True)
	args = parser.parse_args()
	if args.good:
		pprint.pprint(extractor.Extractor(None).loadfile("goodsessions.json"))
	elif args.sessionscores:
		e =  extractor.Extractor(None).sessionscores()
		print e
		print e.loc[1602]
	elif args.phases:
		for s in args.session:
			print "Session "
			print extractor.Extractor(s).phases()
	else:
		for s in args.session:
			print "Session",s
			e = extractor.Extractor(s)

			# We can easily do grouping over a single variable, but we would like to do it by all
			# the variables produced. 

			if args.byphase:
				g = extractor.PhaseGrouper(e)
			elif args.bywindow:
				g = extractor.WindowGrouper(e,args.bywindow*1000.0,skipempty=args.skipempty)
			elif args.bywindowover:
				g = extractor.OverlapGrouper(e,args.bywindowover[0]*1000.0,args.bywindowover[1]*1000.0,skipempty=args.skipempty)
			else:
				g = extractor.NoGrouper(e)

			# example of median of programming time
			if False:
				z = np.diff([x.time for i,x in e.ides().iterrows()])
				z = z[z > 0]
				pt = np.median(z)
				print "median threshold",pt
				for p,v in g.iter(e.faces()):
					q = extractor.TimeTool(threshold=pt)
					q.run(v)
					print "group",p,len(v),q.active,q.total

			allhands = e.handset()
			print s,allhands
			qq = e.handsmerge()
			for p,v in g.iter(e.handsmerge()):
				#q = extractor.TimeTool(threshold=pt)
				#q.run(v)
				#print p,[x().count for x in v["data"]]
				print [len(x) for x in v["data"]]





			#for p,v in g.iter(e.buttons()):
			#	print p,"button",v
			#print e.handpos()
			#print e.faces()
			#print "button",e.buttons()
			#print "audios",e.audios()
			#print e.ides()print "types",e.datatypes()
			#print "faces",e.faceset()
			#print "hands",e.handset()
			#print "usergens",e.usergens()
			# print "phases",e.phases()

			#