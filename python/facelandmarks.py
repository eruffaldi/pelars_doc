#!/usr/bin/python
# The contents of this file are in the public domain. See LICENSE_FOR_EXAMPLE_PROGRAMS.txt
#
#   This example program shows how to find frontal human faces in an image and
#   estimate their pose.  The pose takes the form of 68 landmarks.  These are
#   points on the face such as the corners of the mouth, along the eyebrows, on
#   the eyes, and so forth.
#
#   This face detector is made using the classic Histogram of Oriented
#   Gradients (HOG) feature combined with a linear classifier, an image pyramid,
#   and sliding window detection scheme.  The pose estimator was created by
#   using dlib's implementation of the paper:
#      One Millisecond Face Alignment with an Ensemble of Regression Trees by
#      Vahid Kazemi and Josephine Sullivan, CVPR 2014
#   and was trained on the iBUG 300-W face landmark dataset.
#
#   Also, note that you can train your own models using dlib's machine learning
#   tools. See train_shape_predictor.py to see an example.
#
#   You can get the shape_predictor_68_face_landmarks.dat file from:
#   http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2
#
# COMPILING/INSTALLING THE DLIB PYTHON INTERFACE
#   You can install dlib using the command:
#       pip install dlib
#
#   Alternatively, if you want to compile dlib yourself then go into the dlib
#   root folder and run:
#       python setup.py install
#   or
#       python setup.py install --yes USE_AVX_INSTRUCTIONS
#   if you have a CPU that supports AVX instructions, since this makes some
#   things run faster.  
#
#   Compiling dlib should work on any operating system so long as you have
#   CMake and boost-python installed.  On Ubuntu, this can be done easily by
#   running the command:
#       sudo apt-get install libboost-python-dev cmake
#
#   Also note that this example requires scikit-image which can be installed
#   via the command:
#       pip install scikit-image
#   Or downloaded from http://scikit-image.org/download.html. 

import sys
import os
import dlib
import glob
import cv2
import imageio
import yaml
import numpy as np
import math
import transformations

if len(sys.argv) != 3:
    print(
        "Give the path to the trained shape predictor model as the first "
        "argument and then the directory containing the facial images.\n"
        "For example, if you are in the python_examples folder then "
        "execute this program by running:\n"
        "    ./face_landmark_detection.py shape_predictor_68_face_landmarks.dat ../examples/faces\n"
        "You can download a trained facial shape predictor from:\n"
        "    http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2")
    exit()

def loadcalib(c):
    x = yaml.load(open(c,"rb"))
    K = np.reshape(np.array(x["rgb_intrinsics"]["data"]),(3,3))
    dist = np.reshape(np.array(x["rgb_distortion"]["data"],dtype=np.float64),(1,-1))
    return dict(K=K,dist=dist,width=x["width"],height=x["height"])

def landmark2gaze(l,ci):
    expand = lambda q: (q.x,q.y)
    #https://ibug.doc.ic.ac.uk/resources/facial-point-annotations/
    x = [expand(l.part(i)) for i in [34,9,43,40,55,49]]
    image_points = np.array(x,dtype=np.float64)

    mouthsep = 0.03
    eyesep = 0.018

    eyefromnose = 0.017
    mouthfromnose = -0.015
    chinfromnose = -0.033

    chindepth = -0.01
    eyedepth = -0.02
    mouthdepth = -0.01

    model_points = np.array([
                            (0.0, 0.0, 0.0),             # Nose tip
                            (0.0, chinfromnose, chindepth),        # Chin
                            (-eyesep/2, eyefromnose, eyedepth),     # Left eye left corner
                            (eyesep/2, eyefromnose, eyedepth),      # Right eye right corne
                            (-mouthsep/2, mouthfromnose,mouthdepth),    # Left Mouth corner
                            (mouthsep, mouthfromnose, mouthdepth)      # Right mouth corner
                         
                        ])
    (success, rotation_vector, translation_vector) = cv2.solvePnP(model_points, image_points, ci["K"], ci["dist"],flags=cv2.CV_ITERATIVE)
    print "rotation vector is",rotation_vector
    M = transformations.euler_from_matrix(cv2.Rodrigues(rotation_vector)[0],'rxyz')
    return (success, rotation_vector, translation_vector,[180/math.pi*x for x in M])


def main():

    c920calib = os.path.abspath("c920_800.yml")
    calib = loadcalib(c920calib)
    print calib["K"]
    predictor_path = sys.argv[1]
    faces_folder_path = sys.argv[2]


    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(predictor_path)
    win = dlib.image_window()
    #reader = imageio.get_reader("imageio:" + faces_folder_path)
    reader = cv2.VideoCapture(faces_folder_path)

    objectPoints = np.array([(0,0,0),(0.1,0,0),(0,0.1,0),(0,0,0.1)])
    #for f in glob.glob(os.path.join(faces_folder_path, "*.jpg")):
    while True:
        ret,img = reader.read()#reader.get_next_data()
        if img is None:
            break
        img = img[:,:,(2,1,0)]
        #img = cv2.flip(img,1)
        win.clear_overlay()
        img2 = img.copy()

        # Ask the detector to find the bounding boxes of each face. The 1 in the
        # second argument indicates that we should upsample the image 1 time. This
        # will make everything bigger and allow us to detect more faces.
        points = [34,9,43,40,55,49]
        dets = detector(img, 1)
        for k, d in enumerate(dets):
            shape = predictor(img, d)
            for i in points:
                p = shape.part(i-1)
                cv2.circle(img2, (p.x,p.y), 5,(255,0,0))

            shape = predictor(img, d)
            (success,r,t,e) = landmark2gaze(shape,calib)

            # reproject the points ove rimae using opencv
            imgp, J = cv2.projectPoints(objectPoints, r, t, calib["K"], calib["dist"])
            xpoints = [(int(imgp[q,0,0]),int(imgp[q,0,1])) for q in range(0,imgp.shape[0])]
            cv2.line(img2,xpoints[0],xpoints[1],(255,0,0),1)
            cv2.line(img2,xpoints[0],xpoints[2],(0,255,0),1)
            cv2.line(img2,xpoints[0],xpoints[3],(0,0,255),1)


        win.set_image(img2)

        print("Number of faces detected: {}".format(len(dets)))
        for k, d in enumerate(dets):
            print("\n")
            print("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
                k, d.left(), d.top(), d.right(), d.bottom()))
            # Get the landmarks/parts for the face in box d.
            shape = predictor(img, d)
            print("Part 0: {}, Part 1: {} ...".format(shape.part(0),
                                                      shape.part(1)))
            # Draw the face landmarks on the screen.
            win.add_overlay(shape)

            #points = (0,0,0) ... +10cm per ogni asse
            #passo rvec e tvec da pose estimator
            #



        win.add_overlay(dets)
        dlib.hit_enter_to_continue()


if __name__ == '__main__':
    main()