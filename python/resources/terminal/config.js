require.config({
	paths: {
		vis : 'vis/dist/vis',
		timeline: 'timeline',
		faces_vis:'faces_vis',
		requirejs: 'requirejs/require',
		jquery: 'jquery/dist/jquery',
		jqueryui: 'jquery-ui/jquery-ui',
		gridster: 'gridster/dist/jquery.gridster',
		threeD : 'threeD',
		FontUtils: 'FontUtils',
		TextGeometry: 'TextGeometry',
		OrbitControls: 'OrbitControls',
		'font.Helvetica': 'fonts/helvetiker_regular.typeface',
		'dat.gui': 'dat.gui/dat.gui',
		'threejs' : 'three.js/build/three.min',
		TrackballControls: 'TrackballControls'
		//	d3: 'd3/d3',
		//	datacard: 'data-card',
		//	datacardtest: 'data-card-test'
	}
}
);
