
function timeConverter(UNIX_timestamp){
  var a = new Date( Math.round(UNIX_timestamp));
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = ""+(hour < 10 ? "0":"") + hour + ':' + (min < 10 ?"0": "") + min + ':' + (sec < 10 ? "0": "") + sec ;
  return time;
}

phasesmap ={};
phasesmap["obs_plan"] = "Planning"
phasesmap["obs_document"] = "Documenting"
phasesmap["obs_reflect"] = "Reflect"
phasesmap["obs_rate"] = "Rate"
phasesmap["reflect"] = "Reflect"
phasesmap["document"] = "Documenting"
phasesmap["setup"] = "Setup"
phasesmap["collector"] = "Collector"
function getParam ( sname )
{
  var params = location.search.substr(location.search.indexOf("?")+1);
  var sval = "";
  params = params.split("&");
    // split param and value into individual pieces
    for (var i=0; i<params.length; i++)
       {
         temp = params[i].split("=");
         if ( [temp[0]] == sname ) { sval = temp[1]; }
       }
  return sval;
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}


function generator_step(multimedia_content,phases,phase,phaserel,mediaindex,failcount,lasttime)
{
	while(phase < phases.length)
	{
		var ph = $("#"+phases[phase]["phase"])
		var mc = multimedia_content[mediaindex]
		if(mc == undefined)
			break
		if(mc["time"] <= phases[phase]["end"] && mediaindex < multimedia_content.length)
		{
			var st;
			if((mc["time"]-lasttime) > 3000)
			{
				st = timeConverter(mc["time"]) + "<br/>"
				lasttime = mc["time"]
			}
			else
			{
				st = ""
			}
		   	if(mc["type"] == "text") 
		   	{
		   		var success = function(content) {
					if(phaserel == 0)
						$("body").append("<div class=\"phase\" id=\"" + phases[phase]["phase"] + "\"><span class=\"phase\">" + phasesmap[phases[phase]["phase"]] + "</span></div>");
		   			ph.append("<p><div class='content'>" + st + " - " + content+"</div></p>");
		   			generator_step(multimedia_content,phases, phase, phaserel+1, mediaindex+1,0,lasttime)
		   		}
		   		var url = "multimedia/"+session+"/"+mc["id"]

		   		$.get(url, success).fail(
			   			function(content) 
			   			{	  
			   				if(failcount > 1)
			   				{
			   					// notify failure
				   				generator_step(multimedia_content,phases, phase, phaserel+1, mediaindex+1,0,lasttime)
			   				}
			   				else
			   				{
				   				console.log("retry ","multimedia/"+session+"/"+multimedia_content[mediaindex]["id"])
				   				generator_step(multimedia_content,phases, phase, phaserel, mediaindex,failcount+1,lasttime)
				   			}
				   		}
				   	);
		   		return;
		   	}
		   	else
		   	{
				if(phaserel == 0)
					$("body").append("<div class=\"phase\" id=\"" + phases[phase]["phase"] + "\"><span class=\"phase\">" + phasesmap[phases[phase]["phase"]] + "</span></div>");
		   		if(multimedia_content[mediaindex]["type"] == "image")
		   		{
		   				ph.append("<p>" + st + " " + "<img class=\"image\" src=\"" + mc["data"] + "\"</img></p>");
		   		}
		   		mediaindex += 1
		   		phaserel += 1		   		
		   	}
		}
		else
		{
			phaserel = 0
			phase += 1
		}
	}
}


function create_story()
{
	$.getJSON("/pelars/phase/"+session, function(parsed_phases) {
		$.getJSON("/pelars/multimedia/"+session, function(parsed_muiltimedia) {

		generator_step(parsed_muiltimedia,parsed_phases,0,0,0,0,"")	
		});
	});
}

var session = getParam("session")
if(session == ""){
		alert("call with session as parameter")
}else{
	create_story();
}

