import sys,json,os

def main():
	sid = int(sys.argv[2])
	base = sys.argv[1]
	k = "S%d_multimedias.json" % sid
	fp = os.path.join(base,k)
	md = os.path.join(base,"S%d" % sid)
	mm = json.load(open(fp,"rb"))
	z = []
	for k in mm:
		if k["view"] != "workspace":
			continue
		z.append("file " + os.path.join(md,"media/%d.%s" % (k["id"], k["mimetype"])) )
		#z.append("duration " + str((k["time"]-mm[0]["time"])/1E3))
	print "#done",len(z)
	open("S%d_workspace.concat" % sid,"w").write("\n".join(z))
	print "ffmpeg -f concat -r 1 -i S%d_workspace.concat -q:v 0 S%d.mp4" % (sid,sid)
if __name__ == '__main__':
	main()