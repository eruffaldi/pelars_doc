# PELARS mock - Emanuele Ruffaldi 2016
#
# TODO: full sessions from sesssions.json vs os.listdir
#
#
# POST password
#   user=
#   pwd=
#   Result JSON with "token" 
# GET session/sid
#   Result JSON
# GET session/
#   Result JSON
# GET data/sid/type
#   Result JSON
# GET data/sid  --- NOT IMPLEMENTED
# GET phase/sid

import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path
import thread
import uuid
import json

from tornado.options import define, options

define("port", default=8002, help="run on the given port", type=int)
define("path", default="/Volumes/BigData/PELARS/sessions", help="path",type=str)

basepath = "."
refurl = "*"
class BaseHandler(tornado.web.RequestHandler):
    def check_origin(self, origin):
        print "check origin tordnado"
        return True
    def xxset_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', refurl)
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Max-Age', 1000)
        #self.set_header('Access-Control-Allow-Headers', 'origin, x-csrftoken, content-type, accept')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Content-type', 'application/json')
        print "set header tornado"
    def servesessionfile(self,sid,format):
        self.xxset_default_headers()
        sf = os.path.join(basepath,format % sid)
        print "serving",sf
        if not os.path.isfile(sf):
            self.send_error(404)
        else:
            self.set_header("Content-type","Application/json")
            self.write(open(sf,"rb").read())

class SessionHandler(BaseHandler):
    def get(self,sid):
        self.servesessionfile(sid,"S%s_session.json")

class SessionAllHandler(BaseHandler):
    def get(self):
        self.xxset_default_headers()
        r = "["+",".join([open(os.path.join(basepath,y),"rb").read() for y in os.listdir(basepath) if y[0] == "S" and y.endswith("_session.json")])+"]"
        self.set_header("Content-type","Application/json")
        self.write(r)

class PasswordHandler(BaseHandler):
    #user and pwd
    #fake token
    def post(self):
        self.xxset_default_headers()
        user = self.request.arguments["user"]
        pwd = self.request.arguments["pwd"]
        if not user or not pwd:
            self.send_error(400)
        else:
            self.set_header("Content-type","Application/json")
            self.write(json.dumps(dict(token="1234")))

class PhasesHandler(BaseHandler):
    def get(self,sid):
        self.servesessionfile(sid,"S%s_phases.json")

class DataTypeHandler(BaseHandler):
    def get(self,sid,type):
        self.xxset_default_headers()        
        sf = os.path.join(basepath,"S%s_datas.json" % sid)
        print("lookup ",sf )
        if not os.path.isfile(sf):
            self.send_error(404)
        else:
            self.set_header("Content-type","Application/json")
            q = json.load(open(sf,"rb"))
            q = [x for x in q if x["type"] == type]
            self.write(json.dumps(q))

class DataHandler(BaseHandler):
    def get(self,sid):
        self.servesessionfile(sid,"S%s_datas.json")

class ContentHandler(BaseHandler):
    def get(self,sid):
        self.servesessionfile(sid,"S%s_content.json")


class MultimediasHandler(BaseHandler):
    def get(self,sid):
        self.servesessionfile(sid,"S%s_multimedias.json")

def guessext(x):
    mimes = [("text","plain"),("image","png"),("image","jpg")]
    for ct,e in mimes:
        q = x+"."+e
        if os.path.isfile(q):
            return q,ct+"/"+e
    return None

class MultimediaHandler(BaseHandler):
    def get(self,sid,mid):
        self.xxset_default_headers()
        e = guessext(os.path.join(basepath,"S%s/media/%s" % (sid,mid)))
        if e is None:
            self.send_error(404)
        else:
            self.set_header("Content-type",e[1])
            self.write(open(e[0],"rb").read())

#TODO
class SnapshotHandler(BaseHandler):
    def get(self,sid,time):
        self.xxset_default_headers()
        #e = guessext(os.path.join(basepath,"S%s/media/%s" % (sid,mid)))
        self.send_error(404)

class MultimediaThumbHandler(BaseHandler):
    def get(self,sid,mid):
        self.xxset_default_headers()
        e = guessext(os.path.join(basepath,"S%s/thumb/%s" % (sid,mid)))
        if e is None:
            self.send_error(404)
        else:
            self.set_header("Content-type",e[1])
            self.write(open(e[0],"rb").read())

class NoCacheStaticHandler(tornado.web.StaticFileHandler):
    def set_extra_headers(self, path):
        self.set_header('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0')

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/session/(.+)", SessionHandler),
            (r"/session/", SessionAllHandler),
            (r"/password", PasswordHandler),
            (r"/phase/(.+)", PhasesHandler),
            (r"/data/(.+)/(.+)", DataTypeHandler),
            (r"/data/(.+)", DataHandler),
            (r"/content/(.+)", ContentHandler),
            (r"/multimedia/(.+)/(.+)/thumb", MultimediaThumbHandler),
            (r"/multimedia/(.+)/(.+)", MultimediaHandler),
            (r"/multimedia/(.+)", MultimediasHandler),
            (r"/snapshot/(.+)/(.+)", SnapshotHandler),

            (r"/pelars/session/(.+)", SessionHandler),
            (r"/pelars/session/", SessionAllHandler),
            (r"/pelars/password", PasswordHandler),
            (r"/pelars/phase/(.+)", PhasesHandler),
            (r"/pelars/data/(.+)/(.+)", DataTypeHandler),
            (r"/pelars/data/(.+)", DataHandler),
            (r"/pelars/content/(.+)", ContentHandler),
            (r"/pelars/multimedia/(.+)/(.+)/thumb", MultimediaThumbHandler),
            (r"/pelars/multimedia/(.+)/(.+)", MultimediaHandler),
            (r"/pelars/multimedia/(.+)", MultimediasHandler),
            (r"/pelars/snapshot/(.+)/(.+)", SnapshotHandler),
            #(r"/what", ChatSocketHandler),
            (r"/(.*)", NoCacheStaticHandler, {"path": "./resources"}),
        ]
        settings = dict(
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            template_path=os.path.join(os.path.dirname(__file__)),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=False, #??
        )
        super(Application, self).__init__(handlers, **settings)



class ChatSocketHandler(tornado.websocket.WebSocketHandler):
    waiters = set()
    cache = []
    cache_size = 200
    def check_origin(self, origin):
        return True
    def set_default_headers(self):
            self.set_header('Access-Control-Allow-Origin', refurl)
            self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
            self.set_header('Access-Control-Max-Age', 1000)
            #self.set_header('Access-Control-Allow-Headers', 'origin, x-csrftoken, content-type, accept')
            self.set_header('Access-Control-Allow-Headers', '*')
            self.set_header('Content-type', 'application/json')

    def get_compression_options(self):
        # Non-None enables compression with default options.
        return {}

    def open(self):
        ChatSocketHandler.waiters.add(self)
        def runme(ws,x,more):
            more -= 1
            if more <= 0:
                return
            ws.write_message(dict(type="pos",position=[x,0,0]))
            x += 2.0
            tornado.ioloop.IOLoop.current().call_later(1, runme, self,x,more)


        tornado.ioloop.IOLoop.current().call_later(1, runme,self,0.0,60)

    def on_close(self):
        ChatSocketHandler.waiters.remove(self)

    @classmethod
    def update_cache(cls, chat):
        cls.cache.append(chat)
        if len(cls.cache) > cls.cache_size:
            cls.cache = cls.cache[-cls.cache_size:]

    @classmethod
    def send_updates(cls, chat):
        logging.info("sending message to %d waiters", len(cls.waiters))
        for waiter in cls.waiters:
            try:
                waiter.write_message(chat)
            except:
                logging.error("Error sending message", exc_info=True)

    def on_message(self, message):
        """logging.info("got message %r", message)
        parsed = tornado.escape.json_decode(message)
        chat = {
            "id": str(uuid.uuid4()),
            "body": parsed["body"],
            }
        chat["html"] = tornado.escape.to_basestring(
            self.render_string("message.html", message=chat))

        ChatSocketHandler.update_cache(chat)
        ChatSocketHandler.send_updates(chat)"""
        pass


def main():
    global basepath
    tornado.options.parse_command_line()
    basepath = options.path
    app = Application()
    app.listen(options.port)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()