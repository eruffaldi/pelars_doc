#Sessions

Sessions get accessed with a GET request to the url <http://pelars.sssup.it/pelars/session/[session_id]>. If the session_id part is present the JSON of the session is retrieved otherwise the list of all session is provided. The fields are:

* session identifier
* duration (ms)
* start/end as date time
* institution_name/institution_address
* user
* optional description
* optional score

The JSON content is as follows:

```json
{
	"duration":14396,
	"start":"2016/01/08 15:43:25",
	"end":"2016/01/08 15:43:39",
	"institution_address":"test_address",
	"session":676,
	"user":"nils.ehrenberg@mah.se",
	"institution_name":"test_institution"
}
```
Note: the user ACL can limit the listing and the access to sessions data

#Session Data

Data of session (e.g 615) can be accessed via: <http://pelars.sssup.it/pelars/data/615>

Session data can also be split by type, e.g. in order to get only hands of session 615: <http://pelars.sssup.it/pelars/data/615/hand>

Web interface for viewing the session is: <http://pelars.sssup.it/pelars/view.html#data/615>