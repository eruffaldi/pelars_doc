#Visualization

DIfferent visualizations have been developed:

* Piechart : this represents a piechart where each "slice" represents the duration of a phase. By hovering over the phase with the mouse you will see the phase name. The bars represent multimedia content and are clickable, redirecting to the content.   http://pelars.sssup.it/pelars/piechart.html?session={id}
* 3D Viewer : this represents the face,hand and camera position in a unique 3D space. It shows the posistion of all components over time calibrated at the beginning of the pelars setup. http://pelars.sssup.it/pelars/3d_viewer.html?session={id}
* Data card : this represents a data card with different learing analytics like hand activity and ARduino component connections.  http://pelars.sssup.it/pelars/wholeCard.html?session={id}
* Timeline : this represents all the pelar session data on a timeline which is draggable and cliccable. http://pelars.sssup.it/pelars/timeline.html?session={id}
* Audio: this is the plot of the intensity of all the audio samples recorded during a session. It is also possible to plot a constant threshold by specifying it as an html parameter.  http://pelars.sssup.it/pelars/audio.html?session={id}&thresh={th}
* IDE activity: this plots the number of IDE samples recorded in intervals of 60 seconds. http://pelars.sssup.it/pelars/program.html?session={id}
