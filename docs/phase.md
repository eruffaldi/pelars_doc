#Phases

Phases represent work phases in the pelars session. 
Each phase contains the following fields:

* data_id : a unique id of the phase
* start : the start time in epoch
* end : the end time in epoch
* session
* phase : a string describing the phase

#Example

```
{
    "data_id": 971,
    "end": 1455624511611,
    "phase": "setup",
    "session": 1042,
    "start": 1455624165930
}


```